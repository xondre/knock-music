/**
 * Author: Karel Ondřej
 * Date: 27. 02. 2018
 * Description: ... 
 */

#include "Wtv020sd16p.h"    // nutno stahnout (odkaz nize)

/*
 * Piny na kterych jsou pripojene narazove senzory
 * viz http://henrysbench.capnfatz.com/henrys-bench/arduino-sensors-and-input/keyes-ky-031-arduino-knock-impact-sensor-manual-and-tutorial/
 */
const int SENSOR_A = 10;
const int SENSOR_B = 11;
// const int SENSOR_C = ...;

/*
 * Poradi pisnicek na SD karte (0 => 0000.wav, 1 => 0001.wav, ...)
 */
const int SOUND_A = 0;
const int SOUND_B = 1;
// const int SOUND_C = ...;

/*
 * Piny na kterych je pripojen mp3 modul WTV020-SD-16P
 * viz http://electronoobs.com/eng_arduino_tut8.php
 */
const int RESET = 4;
const int CLOCK = 5;
const int DATA = 6;
const int BUSY = 7;

Wtv020sd16p wtv020sd16p(RESET, CLOCK, DATA, BUSY);

/*
 * Inicializace
 */
void setup() {
    // inicializace seriove linky
    Serial.begin(9600);

    // inicializace pinu s cidly
    pinMode(SENSOR_A, INPUT);
    pinMode(SENSOR_B, INPUT);
    // pinMode(SENSOR_C, INPUT);
  
    // inicializace mp3 modulu
    wtv020sd16p.reset();
}

/*
 * Po sepnuti cidla prehraje konkretni pisnicku. Prehravani by se dalsim sepnutim cidla nemelo prerusit.
 */
void loop() {
    if (digitalRead(SENSOR_A) == LOW) {
        Serial.println("Play sound A.");
        wtv020sd16p.playVoice(SOUND_A);
    } else if (digitalRead(SENSOR_B) == LOW) {
        Serial.println("Play sound B.");
        wtv020sd16p.playVoice(SOUND_B);
    }
    // else if (...) { ... } ...
}
